import React from 'react'
import Footer from '../components/Footer'
import NavBar from '../components/NavBar'
import SignUpForm from './SignUpForm'

function inscription() {
    return (
        <div>
            <NavBar/>
            <SignUpForm/>
            <Footer/>
        </div>
    )
}

export default inscription
