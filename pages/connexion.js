import React from 'react'
import Footer from '../components/Footer'
import LoginForm from '../components/LoginForm'
import NavBar from '../components/NavBar'

function connexion() {
    return (
        <div className="border-b-indigo-600">
            <NavBar/>
            <LoginForm/>
            <Footer/>
        </div>
    )
}

export default connexion
