import React from 'react'
import Footer from '../components/Footer'
import NavBar from '../components/NavBar'
import Profile from '../components/Profile'

function account() {
    return (
        <div>
            <NavBar/>
            <Profile/>
            <Footer/>
        </div>
    )
}

export default account
