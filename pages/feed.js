import axios from 'axios'
import React from 'react'
import CardsFeed from '../components/CardsFeed'
import Footer from '../components/Footer'
import NavBar from '../components/NavBar'

function feed({projets}) {
    return (
        <div>
            <NavBar/>
            <CardsFeed projets={projets}/>
            <Footer/>
        </div>
    )
}

export default feed

export async function getServerSideProps() {

    const resp = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/project");
    console.log(resp.data)
return {
    
    props : {
        projets: resp.data
        
    }
    
}
}