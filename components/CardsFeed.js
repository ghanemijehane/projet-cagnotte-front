import React from 'react'
import Cards from './Cards'

function CardsFeed({projets}) {
    return (
        <div className="grid sm:grid md:grid-cols-2 xl:grid-cols-3">
            {projets.map(({id,dateCreation,description,dateEnd,name,image,category,minAmount, totalAmount})=>(
                <Cards
                key={id}
                id={id}
                dateCreation={dateCreation}
                description={description}
                dateEnd={dateEnd}
                name={name}
                image={image}
                category={category}
                minAmount={minAmount}
                totalAmount={totalAmount}
                />
            ))}
        </div>
    )
}

export default CardsFeed
