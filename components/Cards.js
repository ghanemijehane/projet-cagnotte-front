import axios from 'axios';
import moment from 'moment';
import React from 'react'

function Cards({ projets }) {
console.log("Categorie"+projets.owner)
  return (
    <div className="grid grid-cols-3 mx-auto">
     
      {projets.map((item) => (
 <a
 href={"/project/" + item.id}
 class="block p-4 rounded-lg shadow-sm shadow-indigo-100"
>
 <img
   alt="123 Wallaby Avenue, Park Road"
   src={item.image}
   class="object-cover w-full h-56 rounded-md"
 />

 <div class="mt-2">
   <dl>
     <div>
       <dt class="sr-only">
         Prix
       </dt>

       <dd class="text-sm text-gray-500">
         {item.totalAmount} €
       </dd>
     </div>

     <div className="flex justify-between">
       <dt class="sr-only">
         Adress
       </dt>

       <dd class="font-medium">
         {item.name}
       </dd>
     </div>
     <strong class="border border-red-500 text-white bg-red-500 uppercase px-5 py-1.5 rounded-full text-[10px] tracking-wide">
         {item.category.label}
                       </strong>
     {/* <div>
       {projets.map((item) => (
        <strong class="border border-red-500 text-white bg-red-500 uppercase px-5 py-1.5 rounded-full text-[10px] tracking-wide">
         {item.category.id}
                       </strong>
       ))}
       
     </div> */}
   </dl>

   <dl class="flex items-center mt-6 space-x-8 text-xs">
     <div class="sm:inline-flex sm:items-center sm:shrink-0">
       <svg
         class="w-4 h-4 text-indigo-700"
         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
       >
         <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
       </svg>

       <div class="sm:ml-3 mt-1.5 sm:mt-0">
         <dt class="text-gray-500">
           Participants
         </dt>

         <dd class="font-medium">
           24
         </dd>
       </div>
     </div>

     <div class="sm:inline-flex sm:items-center sm:shrink-0">
       <svg
         class="w-4 h-4 text-indigo-700"
         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
       >
         <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z" />
       </svg>

       <div class="sm:ml-3 mt-1.5 sm:mt-0">
         <dt class="text-gray-500">
           Montant actuel
         </dt>

         <dd class="font-medium">
           2000 €
         </dd>
       </div>
     </div>

     <div class="sm:inline-flex sm:items-center sm:shrink-0">
       <svg
         class="w-4 h-4 text-indigo-700"
         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
       >
         <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" />
       </svg>

       <div class="sm:ml-3 mt-1.5 sm:mt-0">
         <dt class="text-gray-500">
           Date
         </dt>

         <dd class="font-medium">
           {moment(item.dateCreation).format('DD/MM/YYYY')}

         </dd>
       </div>
     </div>
   </dl>
 </div>
</a>

      ))}
     

    </div>
  )
}

export default Cards
export async function getServerSideProps() {

  const resp = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/project");
  console.log(resp.data)



return {
  
  props : {
      projets: resp.data
      }
  
}
}